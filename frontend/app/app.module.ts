import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AppComponent } from './app.component';
import { AppRouterModule } from './app.router';
import { HomepageComponent } from './components/homepage/homepage.component';
import { OrderListComponent } from './components/orders/order-list/order-list.component';
import { OrderService } from './services/order/order.service';
import { CoreModule } from './core/core.module';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    OrderListComponent
  ],
  imports: [
    BrowserModule,
    AppRouterModule,
    HttpClientModule,
    CoreModule,
    AngularFontAwesomeModule
  ],
  providers: [OrderService, HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule {}
