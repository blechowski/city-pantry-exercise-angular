import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { OrderListComponent } from './order-list.component';
import { OrderService } from '../../../services/order/order.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('OrderListComponent', () => {
  let component: OrderListComponent;
  let fixture: ComponentFixture<OrderListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        OrderListComponent
      ],
      providers: [
        OrderService,
      ],
      imports: [HttpClientTestingModule]
    })
    .compileComponents();
    TestBed.createComponent(OrderListComponent).detectChanges();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it(`should create`, async(inject([HttpTestingController, OrderService],
    (httpClient: HttpTestingController, orderListComponent: OrderListComponent) => {
      expect(orderListComponent).toBeTruthy();
  })));

  it('should contain an order table', () => {
    const table = fixture.debugElement.query(By.css('table'));
    expect(table).toBeTruthy();
  });

  it(`should format a date`, () => {
    const date = '2018-08-04T11:30:00.000Z';
    expect(component.formatDate(date)).toEqual('2018-08-04 11:30');
  });
});
