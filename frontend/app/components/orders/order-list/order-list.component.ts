import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../../services/order/order.service';
import moment = require('moment');
import { Order } from '../../../model/order.model';
import { Pager } from '../../../model/pager.model';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.scss']
})
export class OrderListComponent implements OnInit {
  orders: Order[];
  headers = ['customer, requested delivery date, packaging, serving style'];
  pager: Pager = {totalPages: 1, currentPage: 1};

  constructor(
    private orderService: OrderService
  ) {}

  ngOnInit() {
    const defaultPage = 1;
    this.loadData(defaultPage);
  }

  loadData(page: number) {
    if (!page || page > this.pager.totalPages) {
      return;
    }
    this.orderService.getOrdersData(page).subscribe(
      data => {
        const {items, total, pageSize} = data;
        this.orders = items;
        const totalPages = total / pageSize;
        this.setPaginationPager(page, totalPages);
      }
    );
  }

  setPaginationPager(currentPage: number, totalPages: number) {
    this.pager = {
      currentPage,
      totalPages
    };
  }

  formatDate = date => {
    return moment.utc(date).format('YYYY-MM-DD HH:mm');
  }
}
