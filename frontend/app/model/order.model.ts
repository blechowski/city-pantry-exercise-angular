export interface OrderData {
  page: string;
  pagesize: number;
  total: number;
  count: number;
  items: Array <Order>;
}

export interface Order {
  id: number;
  lastModified: string;
  customer: string;
  vendor: string;
  commissionRate: number;
  requestedDeliveryDate: string;
  price: {
    delivery: number;
    items: number;
    total: number;
    vatRate: number;
    vatableItems: number;
    vatAmount: number;
  };
  paymentType: string;
  headcount: number;
  servingStyle: string;
  deliveredAt: string;
  delayMinutes: number;
  lateReason: string;
  packaging: string;
  driverName: string;
  deliveryLocation: {
    lat: number;
    long: number;
  };
  currentLocation: {
    lat: number;
    long: number;
  };
  vendorLocation: {
    lat: number;
    long: number;
  };
}
