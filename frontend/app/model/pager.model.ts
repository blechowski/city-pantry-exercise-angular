export interface Pager {
  currentPage: number;
  totalPages: number;
}
