import { TestBed, inject, async } from '@angular/core/testing';

import { OrderService } from './order.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('OrderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrderService],
      imports: [HttpClientTestingModule]
    });
  });

  it(`should create`, async(inject([HttpTestingController, OrderService],
    (httpClient: HttpTestingController, service: OrderService) => {
      expect(service).toBeTruthy();
  })));
});
