import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { OrderData } from '../../model/order.model';

@Injectable()
export class OrderService {
  private url = 'http://localhost:4300/orders';

  constructor(
    private http: HttpClient
  ) {}

  getOrdersData(page: number): Observable<any> {
    return this.http.get<OrderData>(this.url, {
      params: {
        page: page.toString()
      }
    });
  }
}
